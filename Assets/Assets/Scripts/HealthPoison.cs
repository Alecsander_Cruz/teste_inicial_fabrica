﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPoison : MonoBehaviour

{
    public int health = 10;
    bool poisonEffect = true;
    bool isPoisoned = false;
    public int poisonDamage = 5;
    public float poisonTimer = 1f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(isPoisoned){
            StartCoroutine("PoisonDamage");
        }

        if(health <= 0){
            Destroy(gameObject);
        }


    }

    IEnumerator PoisonDamage(){

                 
        
        if(poisonEffect){
            health -= poisonDamage;
            poisonEffect = false;
            yield return new WaitForSeconds(poisonTimer);
            poisonEffect = true;
        }

    }
}
