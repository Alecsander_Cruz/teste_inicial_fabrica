using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

	public int healthp = 100;

	public GameObject deathEffect;
	bool poisonEffect = true;
bool isPoisoned = false;
public int poisonDamage = 5;
public float poisonTimer = 1f;


void Update() {
	if(isPoisoned){
            StartCoroutine("PoisonDamage");
        }
	if (healthp <= 0)
		{
			Die();
		}	
}

	public void TakeDamage (int damage)
	{
		isPoisoned = true;
		

		if (healthp <= 0)
		{
			Die();
		}
	}

	void Die ()
	{
		Instantiate(deathEffect, transform.position, Quaternion.identity);
		Destroy(gameObject);
	}

	IEnumerator PoisonDamage(){

                 
        
        if(poisonEffect){
            healthp -= poisonDamage;
            poisonEffect = false;
            yield return new WaitForSeconds(poisonTimer);
            poisonEffect = true;
        }

    }

}
