﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoint : MonoBehaviour
{
    private GameController gc;
    void Start()
    {
        gc = GameObject.FindGameObjectWithTag("gameController").GetComponent<GameController>();
    }

    void OnTriggerEnter2D (Collider2D col)
    {
        if(col.CompareTag("Player"))
        {
            gc.lastCheckpointPosition = transform.position;
        }
    }

}
